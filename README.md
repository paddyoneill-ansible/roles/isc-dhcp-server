Ansible Role ISC DHCP Server
============================

Ansible role to deploy and configure ISC DHCP servers.

The Internet Systems Consortium DHCP Server, dhcpd, implements the Dynamic
Host Configuration Protocol (DHCP) and the Internet Bootstrap Protocol (BOOTP).
DHCP allows hosts on a TCP/IP network to request and be assigned IP addresses,
and also to discover information about the network to which they areattached.
BOOTP provides similar functionality, with certain restrictions.

This role is currently tested on the following distributions.

| Distro | Versions                      |
| :---   | :---                          |
| CentOS | <ul><li>7</li><li>8</li></ul> |


Requirements
------------

None

Role Variables
--------------

The following options can be set to configure global options in the DHCP
configuration file. These will apply to any subnet block that does not
have a value set to override them.

| Variable                             | Required | Default | Description                                                                   |
| :---                                 | :---     | :---    | :---                                                                          |
| `isc_dhcp_server_domain_name`        | false    | ""      | Gloabl domain names                                                           |
| `isc_dhcp_server_dns_servers`        | false    | []      | Global DNS serverss                                                           |
| `isc_dhcp_server_default_lease_time` | false    | 7200    | Default lease time in seconds                                                 |
| `isc_dhcp_server_max_lease_time`     | false    | 600     | Max lease time in seconds                                                     |
| `isc_dhcp_server_ddns_update_style`  | false    | none    | The update style to use for DDNS updates                                      |
| `isc_dhcp_server_authoritive`        | false    | false   | Set to true if server is authoritative.                                       |
| `isc_dhcp_server_log_facility`       | false    | local7  | Causes the DHCP server to do all of its logging on the specified log facility |
| `isc_dhcp_server_subnets`            | false    | []      | List of subnets that this DHCP server will serve addresses for                |

Subnets are provided as a list of dicts. At a minimum each subnet must provide
a network IP address and a netmask. Other options can also be passed to
override the global options.

| Variable             | Required | Default   | Description                                            |
| :---                 | :---     | :---      | :---                                                   |
| `ip`                 | true     | undefined | The network IP address of the subnet                   |
| `netmask`            | true     | undefined | The netmask of the subnet                              |
| `range_start`        | false    | undefined | The first IP address of the dynamic range              |
| `range_end`          | false    | undefined | The last IP address of the dynamic range               |
| `domain_name`        | false    | undefined | The domain name to give clients on the subnet          |
| `dns_servers`        | false    | undefined | List of DNS servers to pass to clients                 |
| `routers`            | false    | undefined | The router IP address of the subnet                    |
| `broadcast_address`  | false    | undefined | The broadcast IP address of the subnet                 |
| `default_lease_time` | false    | undefined | Default duration of leases for clients on the subnet   |
| `max_lease_time`     | false    | undefined | Maximimum duration of leases for clients on the subnet |


Dependencies
------------

None

Example Playbook
----------------


    - hosts: servers
      roles:
         - role: paddyoneill.isc_dhcp_server

License
-------

MIT
